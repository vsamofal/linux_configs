
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#  Customize BASH PS1 prompt to show current GIT repository and branch.
#  by Mike Stewart - http://MediaDoneRight.com

#  SETUP CONSTANTS
#  Bunch-o-predefined colors.  Makes reading code easier than escape sequences.
#  I don't remember where I found this.  o_O

# Reset
Color_Off="\[\033[0m\]"       # Text Reset
# Regular Colors
Yellow="\[\033[0;33m\]"       # Yellow
# High Intensty
IBlack="\[\033[0;90m\]"       # Black

# Various variables you might want for your PS1 prompt instead
Time12h="\T"
Time12a="\@"
PathShort="\w"
PathFull="\W"
NewLine="\n"
Jobs="\j"


# This PS1 snippet was adopted from code for MAC/BSD I saw from: http://allancraig.net/index.php?option=com_content&view=article&id=108:ps1-export-command-for-git&catid=45:general&Itemid=96
# I tweaked it to work on UBUNTU 11.04 & 11.10 plus made it mo' better

export PS1=$IBlack$Time12h$Color_Off'$(\
  echo " '$Yellow$PathShort$Color_Off'\$ "; \
)'


alias ls='ls --color=auto'
alias l='ls'
alias ll='ls -lah'

alias grep='grep --color=auto'
alias mkdir='mkdir -p -v'

alias docker='sudo docker'
shopt -s autocd
[ -z "$NVM_DIR" ] && export NVM_DIR="$HOME/.nvm"
#source /usr/share/nvm/init-nvm.sh
